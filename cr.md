Data Package: LANL Chromium site model `Cr3DVA`
==========================================

The model `Cr3DVA` is designed to represent the groundwater flow and contaminant transport in the vadose zone and the regional aquifer at the LANL Chromium contamination site.
Laterally, the model domain includes the Mortandad, Sandia and Los Alamos Canyons.
Vertically, the model extends from ground surface to a distance under the regional water table including the entire the vadose zone.

The major goals of these model analyses are to:
1. Simulate groundwater flow and contaminant transport conditions at the site.
2. Predict the fate of contaminant mass in the vadose in the future, including the future impacts on the regional aquifer concentrations.
3. Represent the system behavior under different future natural and engineered scenarios related to contaminant remediation (including changes in the infiltration fluxes, gas-phase amendment deployment, etc.)

To achieve these goals, a series of model analyses are performed which currently target predominantly Goal #1 above.
The LANL Chromium contamination site is calibrated to:
1. Reproduce the perching of infiltrated groundwater on the top of the basalts within the vadose zone.
2. Match the model predicted saturation thickness with existing observations.
3. Obtain groundwater and contaminant-mass fluxes through the hydraulic windows through the basalts in the vadose zone that are capable to reproduced the groundwater and contaminant-mas fluxes estimated by the large-scale regional aquifer models.
4. Achieve advective travel times through the vadose zone consistent with existing site data and knowledge.

The forward model runs are accomplished using [FEHM](http://fehm.lanl.gov). The inverse analyses are carried out using [Mads](http://mads.lanl.gov).

Computational grid
---
3D model domain and computational grid of the Chromium model:

![grid](./figures/grid.png "3D grid")

Detailed description of the computational grid can be found from [README](https://meshing.lanl.gov/proj/ER_Cr_mesh/images_stack0/README.txt) or [here](https://meshing.lanl.gov/proj/ER_Cr_mesh/Cr_index.html), and the spatial distribution of stratigraphic units can be found [here](https://meshing.lanl.gov/proj/ER_Cr_mesh/images_stack0/gallery.html).


Model setup
-----------
Based on detailed preliminary model analyses considering computational time, convergence, and accuracy, a conceptual model accounting for water-phase only flow and transport based on Richard's Equation was adopted.
van Genuchten relative permeability capillary pressure model is used, and vadose-zone permeabilities of various site units are represented as a function of saturation.

The initial and boundary conditions for the flow simulations are prescribed as follows:
- the entire perched zones are initially assigned to constant hydrostatic pressure head and full saturation;
- the water table locations are fixed by assigning constant hydrostatic pressure head and saturation of equal to one to all nodes within the saturated zone;
- the boundary nodes that are part of perched water zones are assigned to constant hydrostatic pressure head and full saturation;
- permeability reduction was applied to the bottom of the perched zones;
- based on the information from the large-scale regional aquifer models, three hydraulic windows were defined in the vadose zone model, allowing downward water flow through these windows from the perched horizons within the vadose zone towards the regional aquifer.

Transport of Chromium is modeled classical advective dispersion equation (ADE) as defined in  the `trac` macro of the FEHM.
Groundwater flow and contaminant transport simulations are carried out sequentially assuming steady-state groundwater flow conditions.

Model parameters to be estimated
--------------------------------
The model parameters to be estimated include (a total of 20):

- permeability of nodes within the three hydraulic windows (3);
- permeability of nodes above, within, and below the basalt (3);
- porosity of nodes above, within, and below the basalt (3);
- inverse of air entry head for nodes above, within, and below the basalt (3);
- power `n` in van Genuchten formula for nodes above, within, and below the basalt (3);
- infiltration rates at the ground surface (1);
- canyon infiltration flux (3);
- water influx from western boundary (1).


Measurements available
---------------------

- thickness of upper/lower perched zones at 7154 locations, 11 of which used as calibration targets during the model inversion;
- source fluxes estimated by the large-scale regional aquifer models;
- advective travel times through the vadose zone.


Model execution
--------------
The model simulations were performed on LANL HPC clusters.
An inverse model job can be run using the following `julia` command lines:

```julia
md = Mads.loadmadsfile("cr3dva.mads")
p, r = Mads.calibrate(md)
```

where `md` is a Mads data dictionary loaded from an external input file `cr3dva.mads`, `p` and `r` are the model calibration results.

Example model outputs
---------------------
The performance of the inverse model can be evaluated by comparing simulated and observed:
1. perched zone thickness;
2. fluxes through the hydraulic windows; and
3. Chromium travel times through the vadose zone.

The following figure illustrates the simulated perched zones.

![](./figures/perchedzone.png)

The following figure illustrates the simulated perched zones with three hydraulic windows.

![](./figures/hydraulicwindows.png)


The following figure compares the observed and simulated thickness of upper perched zone.

![](./figures/calibratedthickness.png)

The comparisons between the observed and simulated results can be found in this [excel file](https://gitlab.com/lanl/cr3va/blob/master/figures/compasions.xlsx).

Directory structure:
-------

All the files associated with the Chromium model are listed as follows:

```
   |-data/
   |---perm.dat
   |---pres_ss.macro
   |---rlp.dat
   |---rock.dat
   |-grid/
   |---basalt_in.zone
   |---basalt_in.zonn
   |---basalt_out.zonn
   |---basalt_top_abv.zonn
   |---basalt_top_eblw.zonn
   |---basalt_top_eq.zonn
   |---basalt_win1.zonn
   |---basalt_win2.zonn
   |---basalt_win3.zonn
   |---bot_win1.zonn
   |---bot_win2.zonn
   |---bot_win3.zonn
   |---out_bottom.zonn
   |---out_east_right.zonn
   |---out_north_back.zonn
   |---out_north_west.zonn
   |---out_south_west.zonn
   |---out_south.zonn
   |---out_top.zone
   |---out_top.zonn
   |---out_west_left.zonn
   |---pwz_allsides.zonn
   |---tet_ascii.stor
   |---tet.fehmn
   |---tet.geo
   |---tet_interface.zone
   |---tet_material.zone
   |---tet_multi_mat.zone
   |---TOP_ply_lac.zonn
   |---TOP_ply_mortandad.zonn
   |---TOP_ply_sandia_w.zonn
   |---TOP_ply_sandia.zonn
   |---top_win1.zonn
   |---top_win2.zonn
   |---top_win3.zonn
   |---wtr_abv.zone
   |---wtr_abv.zonn
   |---wtr_blw_bot.zonn
   |---wtr_blw_east.zonn
   |---wtr_blw_north.zonn
   |---wtr_blw_nw.zonn
   |---wtr_blw_south.zonn
   |---wtr_blw_sw.zonn
   |---wtr_blw_top.zonn
   |---wtr_blw_west.zonn
   |---wtr_blw.zone
   |---wtr_blw.zonn
   |-codes/
   |---post_process_avs_getpwz.f90
   |---post_process_avs_getpwz.x
   |---read_brk.f90
   |---read_brk.x
   |-setupBoundaries/
   |---basalt_in.zonn
   |---flow_pwz.macro
   |---flow_sat.macro
   |---pres_pwz.macro
   |---pres_sat.macro
   |---pwz_allsides.zonn
   |---pwZone_presFlow_macro.input
   |---pwz_thickness.dat
   |---sat_allsides.zonn
   |---satZone_presFlow_macro.input
   |---write_presFlow_macro_pwz.f90
   |---write_presFlow_macro_pwz.x
   |---write_presFlow_macro_sat.f90
   |---write_presFlow_macro_sat.x
   |-initialRun4PE/
   |---flow.data
   |---flow.files
   |-inverse_model/
   |---brkthr.inst
   |---cr.mads
   |---flow.data.tpl
   |---flow.files
   |---perm.dat.tpl
   |---post_process_avs_getpwz.input
   |---rlp.dat.tpl
   |---rock.dat.tpl
   |---runmodel.sh
   |---thickness.inst
   |---trac.macro
   |---trans.data.tpl
   |---trans.files
   |---winfluxes.inst

```

- `grid` directory contains files related to computational grid including various FEHM `zone` files defining various hydro-stratigraphic units within the model domain.
- `data` directory contains site information.
- `codes` directory contains several FORTRAN codes for post-processing the flow field and concentration output.
- `setupBoundaries` directory contains files for determining boundary nodes and creating FEHM `flow` macro files.
- `initianlRun4PE` directory contains an initial forward model run with initial model parameters; the FEHM `fin` file from this model run will be used as an initial guess for all forward runs in the inverse model.


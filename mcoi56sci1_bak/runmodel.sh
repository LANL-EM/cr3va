#!/usr/bin/env bash

/n/swdev/FEHM_dev/STABLE/bin/xfehm_v3.3.0linUbuntu.18Apr16 h10.files 
/scratch/nts/qkang/ep/bin/post_process_avs_getpwz
cat ./simu_thickness.dat | awk '{print $3}' > thickness
tail -50 h10.outp > tempout1
grep '81 (    416)' tempout1 > tempout2
grep '82 (   1204)' tempout1 >> tempout2
grep '83 (    266)' tempout1 >> tempout2
cat tempout2 | awk '{print $6}' > winfluxes
/n/swdev/FEHM_dev/STABLE/bin/xfehm_v3.3.0linUbuntu.18Apr16 trans_h10.files
/scratch/nts/qkang/ep/bin/post_process_conc.x
/scratch/nts/qkang/ep/bin/read_brk_new
